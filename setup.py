from setuptools import setup, find_packages
setup(
	name="django_herramientas",
	version="0.1",
	packages=find_packages(),
	author="Javiera Encina",
	author_email="jav.encina@gmail.com",
)
